#include <iostream>
#include <queue>
#include <climits>

using namespace std;

const int FRESH = 0;
const int OPEN = 1;
const int CLOSED = 2;
const int MAX_NODES = 1000;

int nodes;
int edges;
int capacity[MAX_NODES][MAX_NODES];
int flow[MAX_NODES][MAX_NODES];     
int status[MAX_NODES];              
int pred[MAX_NODES];  // array to store augmenting path

int bfs(int start, int target)
{
	int tmp;
	queue<int> qu;
   	for(int i=0; i<nodes; i++)
   		status[i] = FRESH;

   	status[start]=OPEN;
    qu.push(start);
    pred[start] = -1;
    while(!qu.empty())
    {
		tmp = qu.front();
		qu.pop();
		for(int i=0; i<nodes; i++)
		{
	    	if(status[i]==FRESH && capacity[tmp][i]-flow[tmp][i]>0)
	    	{
				status[start]=OPEN;
				qu.push(i);
				pred[i] = tmp;
	    	}
		}
		status[tmp]=CLOSED;
    }
    return status[target]==CLOSED;
}

int maxFlow(int source, int sink)
{
    int maxFlow=0;
    for(int i=0; i<nodes; i++)
		for(int j=0; j<nodes; j++)
	    	flow[i][j]=0;

    while(bfs(source,sink))
    {
		int increment = INT_MAX;
		for(int i=nodes-1; pred[i]>=0; i=pred[i])
	    	increment = min(increment,capacity[pred[i]][i]-flow[pred[i]][i]);

		for(int i=nodes-1; pred[i]>=0; i=pred[i])
		{
	    	flow[pred[i]][i] += increment;
	    	flow[i][pred[i]] -= increment;
		}
		maxFlow += increment;
    }
    return maxFlow;
}

void solveProblem()
{
	int start, ends, cost;

	for(int i=0; i<nodes; i++)
		for(int j=0; j<nodes; j++)
			capacity[i][j]=0;

	for(int i=0; i<edges; i++)
	{
		cin >> start >> ends >> cost;
		capacity[start][ends]=cost;
	}
	for(int i=0; i<nodes; i++)
    {
        for(int j=0; j<nodes; j++)
        {
            cout << capacity[i][j] << " ";
        }
        cout << endl;
    }

	cout << maxFlow(0,nodes-1) << endl;

	return;
}

int main()
{
	cin >> nodes >> edges;
	while(nodes && edges)
	{
		solveProblem();
		cin >> nodes >> edges;
	}
	return 0;
}